﻿using BusinessActivities.WebApi.Services;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Net;
using Xunit;

namespace BusinessActivities.UnitTests.Services
{
    public class RequestLogFormatterTests
    {
        [Fact]
        public void Formatter_ReturnsNewRequestLog_ForDefaultHttpContext()
        {
            // Arrange
            var formatter = new RequestLogFormatter();
            var created = DateTime.Now;
            var duration = TimeSpan.FromMilliseconds(20);
            var httpMethod = HttpMethods.Get;
            var statusCode = StatusCodes.Status404NotFound;
            var ipAddress = IPAddress.Parse("127.0.0.1");
            var queryParameter = new KeyValuePair<string, string>("Page", "1");
            var requestHeader = new KeyValuePair<string, StringValues>("Foo", "Bar");
            var responseHeader = new KeyValuePair<string, StringValues>("Server", "Kestrel");
            var route = "/Api";

            var context = new DefaultHttpContext();
            context.Request.Method = httpMethod;
            context.Response.StatusCode = statusCode;
            context.Connection.RemoteIpAddress = ipAddress;
            context.Request.QueryString = QueryString.Create(queryParameter.Key, queryParameter.Value);
            context.Request.Headers.Add(requestHeader);
            context.Response.Headers.Add(responseHeader);
            context.Request.Path += route;

            // Act
            var requestLog = formatter.FormatRequestLog(context, created, duration);

            // Assert
            requestLog.Should().NotBeNull();
            requestLog.Created.Should().Be(created);
            requestLog.Duration.Should().Be(duration.TotalMilliseconds);
            requestLog.HttpMethod.Should().Be(httpMethod);
            requestLog.HttpStatusCode.Should().Be(statusCode);
            requestLog.IPAddress.Should().Be(ipAddress.ToString());
            requestLog.Query.Should().Contain(queryParameter.Key);
            requestLog.Query.Should().Contain(queryParameter.Value);
            requestLog.RequestHeaders.Should().Contain(requestHeader.Key);
            requestLog.RequestHeaders.Should().Contain(requestHeader.Value);
            requestLog.ResponseHeaders.Should().Contain(responseHeader.Value);
            requestLog.ResponseHeaders.Should().Contain(responseHeader.Value);
            requestLog.Route.Should().Contain(route);
        }

        [Fact]
        public void Formatter_ReturnsNewRequestLog_ForDefaultHttpContext_WithNoInitialValues()
        {
            // Arrange
            var formatter = new RequestLogFormatter();
            var created = DateTime.Now;
            var duration = TimeSpan.FromMilliseconds(20);

            var context = new DefaultHttpContext();

            // Act
            var requestLog = formatter.FormatRequestLog(context, created, duration);

            // Assert
            requestLog.Should().NotBeNull();
            requestLog.Created.Should().Be(created);
            requestLog.Duration.Should().Be(duration.TotalMilliseconds);
            requestLog.HttpMethod.Should().BeNullOrEmpty();
            requestLog.HttpStatusCode.Should().Be(StatusCodes.Status200OK);
            requestLog.IPAddress.Should().BeNullOrEmpty();
            requestLog.Query.Should().BeNullOrEmpty();
            requestLog.RequestHeaders.Should().BeNullOrEmpty();
            requestLog.ResponseHeaders.Should().BeNullOrEmpty();
            requestLog.Route.Should().BeNullOrEmpty();
        }
    }
}
