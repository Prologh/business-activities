﻿using BusinessActivities.WebApi.Services;
using FluentAssertions;
using System;
using Xunit;

namespace BusinessActivities.UnitTests.Services
{
    public class NipPatternGeneratorTests
    {
        [Fact]
        public void Generator_ThrowsException_WhenNipIsNull()
        {
            // Arrange
            string nip = null;
            var generator = new NipPatternGenerator();

            // Act && Assert
            Assert.Throws<ArgumentException>(() => generator.GenerateNipPatternWithDashesForBusinesses(nip));
        }

        [Fact]
        public void Generator_ThrowsException_WhenNipIsToShort()
        {
            // Arrange
            var nip = "123";
            var generator = new NipPatternGenerator();

            // Act && Assert
            Assert.Throws<ArgumentException>(() => generator.GenerateNipPatternWithDashesForBusinesses(nip));
        }

        [Fact]
        public void Generator_ThrowsException_WhenNipIsToLong()
        {
            // Arrange
            var nip = "12345678905423543";
            var generator = new NipPatternGenerator();

            // Act && Assert
            Assert.Throws<ArgumentException>(() => generator.GenerateNipPatternWithDashesForBusinesses(nip));
        }

        [Fact]
        public void Generator_ThrowsException_WhenNipContainsNonDigits()
        {
            // Arrange
            var nip = "123abc1234";
            var generator = new NipPatternGenerator();

            // Act && Assert
            Assert.Throws<ArgumentException>(() => generator.GenerateNipPatternWithDashesForBusinesses(nip));
        }

        [Fact]
        public void Generator_ReturnsPatternWithDashesForBusinesses()
        {
            // Arrange
            var nip = "1234567890";
            var generator = new NipPatternGenerator();

            // Act
            var result = generator.GenerateNipPatternWithDashesForBusinesses(nip);

            // Assert
            result.Should().Be("123-45-67-890");
        }

        [Fact]
        public void Generator_ReturnsPatternWithDashesForIndividuals()
        {
            // Arrange
            var nip = "1234567890";
            var generator = new NipPatternGenerator();

            // Act
            var result = generator.GenerateNipPatternWithDashesForIndividuals(nip);

            // Assert
            result.Should().Be("123-456-78-90");
        }

        [Fact]
        public void Generator_ReturnsPatternWithLeadingCountryLetters()
        {
            // Arrange
            var nip = "1234567890";
            var generator = new NipPatternGenerator();

            // Act
            var result = generator.GenerateNipPatternWithLeadingCountryLetters(nip);

            // Assert
            result.Should().Be($"PL{nip}");
        }
    }
}
