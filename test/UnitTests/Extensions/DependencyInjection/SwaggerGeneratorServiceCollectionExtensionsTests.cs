﻿using BusinessActivities.Extensions.DependencyInjection;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using Xunit;

namespace BusinessActivities.UnitTests.Extensions.DependencyInjection
{
    public class SwaggerGeneratorServiceCollectionExtensionsTests
    {
        [Fact]
        public void ExtensionMethod_AddsSwaggerGenerator_WithOptions()
        {
            // Arrange
            var services = new ServiceCollection();
            var preExtensionServiceProvider = services.BuildServiceProvider();

            // Act
            services.AddSwaggerGenWithDefaultOptions();
            var postExtensionServiceProvider = services.BuildServiceProvider();

            // Assert
            preExtensionServiceProvider
                .GetService<IOptions<SwaggerGenOptions>>()
                ?.Value
                .Should()
                .BeNull("Because no Swagger Generator has been registered yet.");
            postExtensionServiceProvider
                .GetService<IOptions<SwaggerGenOptions>>()
                ?.Value
                .Should()
                .NotBeNull("Because extension method should already add " +
                    "Swagger Generator with options to the service collection.");
        }
    }
}
