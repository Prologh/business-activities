﻿using BusinessActivities.Models;
using BusinessActivities.WebApi.Data;
using FluentAssertions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;
using Xunit;

namespace BusinessActivities.IntegrationTests.WebApi.MiddleWare
{
    public class RequestLoggingMiddlewareTests
    {
        public RequestLoggingMiddlewareTests()
        {

        }

        [Fact]
        public async Task MiddleWare_Logs_BusinessActivityRequest()
        {
            // Arrange
            var builder = new WebHostBuilder()
                .ConfigureServices(services =>
                {
                    services.AddEntityFrameworkInMemoryDatabase();

                    // Add a database context using an in-memory database for testing.
                    services.AddDbContext<ApiDbContext>(options =>
                    {
                        options.UseInMemoryDatabase("InMemoryDatabase");
                    });

                    services.Configure<RequestLoggingOptions>(options =>
                    {
                        options.LogLevel = RequestLoggingLevel.BusinessActivityRequests;
                        options.LogOnlySuccessfulRequests = false;
                    });
                })
                .UseStartup<BusinessActivities.WebApi.Startup>();
            var server = new TestServer(builder);
            var client = server.CreateClient();
            client.BaseAddress = new Uri("https://localhost:443");
            var provider = server.Host.Services;
            var dbContext = provider.GetRequiredService<ApiDbContext>();
            var id = "0000045146";
            var uri = $"api/BusinessActivities/{id}";

            // Act
            var response = await client.GetAsync(uri);
            var requestLog = await dbContext.Set<RequestLog>().AsNoTracking().FirstOrDefaultAsync();

            // Assert
            requestLog.Should().NotBeNull();
            requestLog.BusinessActivityId.Should().Be(id);
        }
    }
}
