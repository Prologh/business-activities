﻿using BusinessActivities.WebApi;
using BusinessActivities.WebApi.Data;
using BusinessActivities.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Net.Http;

namespace BusinessActivities.IntegrationTests.WebApi
{
    /// <summary>
    /// Factory for bootstrapping Business Activities Web API in memory
    /// for functional end to end tests.
    /// </summary>
    public class BusinessActivitiesWebApiFactory : WebApplicationFactory<Startup>
    {
        /// <summary>
        /// Creates a new instance of an <see cref="HttpClient"/> that
        /// can be used to send <see cref="HttpRequestMessage"/> to the server.
        /// The base address of the <see cref="HttpClient"/> instance
        /// will be set to https://localhost:443.
        /// </summary>
        /// <returns>
        /// An instance of <see cref="HttpClient"/>.
        /// </returns>
        public virtual HttpClient CreateHttpsClient()
        {
            return CreateClient(new WebApplicationFactoryClientOptions
            {
                BaseAddress = new Uri("https://localhost:443"),
            });
        }

        /// <summary>
        /// Creates a new instance of an <see cref="HttpClient"/> that
        /// can be used to send <see cref="HttpRequestMessage"/> to the server.
        /// The base address of the <see cref="HttpClient"/> instance
        /// will be set to https://localhost:443.
        /// </summary>
        /// <param name="options">
        /// Options used for creation <see cref="HttpClient"/>.
        /// </param>
        /// <returns>
        /// An instance of <see cref="HttpClient"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="options"/> is <see langword="null"/>.
        /// </exception>
        public virtual HttpClient CreateHttpsClient(WebApplicationFactoryClientOptions options)
        {
            if (options == null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            options.BaseAddress = new Uri("https://localhost:443");

            return CreateClient(options);
        }

        /// <summary>
        /// Creates a new instance of an <see cref="HttpClient"/> that
        /// can be used to send <see cref="HttpRequestMessage"/> to the server.
        /// The base address of the <see cref="HttpClient"/> instance
        /// will be set to https://localhost:443.
        /// </summary>
        /// <param name="options">
        /// Action for configuring <see cref="WebApplicationFactoryClientOptions"/>
        /// used for creation <see cref="HttpClient"/>.
        /// </param>
        /// <returns>
        /// An instance of <see cref="HttpClient"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="action"/> is <see langword="null"/>.
        /// </exception>
        public virtual HttpClient CreateHttpsClient(Action<WebApplicationFactoryClientOptions> action)
        {
            if (action == null)
            {
                throw new ArgumentNullException(nameof(action));
            }

            var defaultOptions = ClientOptions;
            defaultOptions.BaseAddress = new Uri("https://localhost:443");
            action(defaultOptions);

            return CreateClient(defaultOptions);
        }

        /// <summary>
        /// Gives a fixture an opportunity to configure the application
        /// before it gets built.
        /// </summary>
        /// <param name="builder">
        /// The <see cref="IWebHostBuilder"/> for the application.
        /// </param>
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                // Create seperate service provider.
                var internalServiceProvider = new ServiceCollection()
                    .AddEntityFrameworkInMemoryDatabase()
                    .BuildServiceProvider();

                // Add a database context using an in-memory database for testing.
                services.AddDbContext<ApiDbContext>(options =>
                {
                    options.UseInMemoryDatabase("InMemoryDatabase");
                    options.UseInternalServiceProvider(internalServiceProvider);
                });

                var serviceProvider = services.BuildServiceProvider();

                // Create a scope to obtain a reference to the database contexts.
                using (var scope = serviceProvider.CreateScope())
                {
                    var scopedServices = scope.ServiceProvider;
                    var appDb = scopedServices.GetRequiredService<ApiDbContext>();

                    appDb.Database.EnsureCreated();

                    try
                    {
                        DatabaseInitializer.Initialize(appDb);
                    }
                    catch (Exception ex)
                    {
                        var logger = scopedServices.GetRequiredService<ILogger<BusinessActivitiesWebApiFactory>>();
                        logger.LogError(ex, "An error occurred while seeding the database.");
                    }
                }

                services.Configure<RequestLoggingOptions>(options =>
                {
                    options.LogLevel = RequestLoggingLevel.BusinessActivityRequests;
                    options.LogOnlySuccessfulRequests = false;
                });
            });
        }
    }
}
