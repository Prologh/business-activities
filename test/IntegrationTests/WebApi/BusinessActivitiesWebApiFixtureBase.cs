﻿using Xunit;

namespace BusinessActivities.IntegrationTests.WebApi
{
    /// <summary>
    /// Provides base fixture for Business Activities Web API integration tests.
    /// </summary>
    public abstract class BusinessActivitiesWebApiFixtureBase : IClassFixture<BusinessActivitiesWebApiFactory>
    {
        /// <summary>
        /// Creates a new instance of <see cref="BusinessActivitiesWebApiFixtureBase"/>
        /// class.
        /// </summary>
        /// <param name="factory">
        /// The Web API factory.
        /// </param>
        public BusinessActivitiesWebApiFixtureBase(BusinessActivitiesWebApiFactory factory)
        {
            Factory = factory;
        }

        /// <summary>
        /// Gets the web application factory.
        /// </summary>
        protected BusinessActivitiesWebApiFactory Factory { get; }
    }
}
