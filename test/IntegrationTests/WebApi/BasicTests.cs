﻿using FluentAssertions;
using System;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace BusinessActivities.IntegrationTests.WebApi
{
    public class BasicTests : BusinessActivitiesWebApiFixtureBase
    {
        public BasicTests(BusinessActivitiesWebApiFactory factory) : base(factory)
        {

        }

        [Fact]
        public async Task Get_EndpointsRedirectHttpRequests()
        {
            // Arrange
            var id = "0000045146";
            var uri = $"api/BusinessActivities/{id}";
            var client = Factory.CreateDefaultClient();

            // Act
            var response = await client.GetAsync(uri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.TemporaryRedirect);
            response.Headers.Location.Scheme.Should().Be(Uri.UriSchemeHttps);
        }

        [Fact]
        public async Task Get_EndpointsReturnCorrectCharset()
        {
            // Arrange
            var krs = "0000045146";
            var uri = $"api/BusinessActivities/{krs}";
            var client = Factory.CreateHttpsClient();

            // Act
            var response = await client.GetAsync(uri);

            // Assert
            response.Content.Headers.ContentType.ToString().Should().Contain("charset=utf-8");
        }

        [Fact]
        public async Task Get_EndpointsReturnCorrectContentType()
        {
            // Arrange
            var id = "0000045146";
            var uri = $"api/BusinessActivities/{id}";
            var client = Factory.CreateHttpsClient();

            // Act
            var response = await client.GetAsync(uri);

            // Assert
            response.Content.Headers.ContentType.ToString().Should().Contain("application/json");
        }

        [Fact]
        public async Task Get_EndpointsReturnSuccessCode()
        {
            // Arrange
            var id = "0000045146";
            var uri = $"api/BusinessActivities/{id}";
            var client = Factory.CreateHttpsClient();

            // Act
            var response = await client.GetAsync(uri);

            // Assert
            response.IsSuccessStatusCode.Should().BeTrue();
        }
    }
}
