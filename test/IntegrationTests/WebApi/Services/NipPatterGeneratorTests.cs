﻿using BusinessActivities.Models;
using BusinessActivities.WebApi.Services;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Xunit;

namespace BusinessActivities.IntegrationTests.WebApi.Services
{
    public class NipPatterGeneratorTests
    {
        private readonly IQueryable<BusinessActivity> _businessActivities;

        public NipPatterGeneratorTests()
        {
            _businessActivities = new BusinessActivity[]
            {
                new BusinessActivity
                {
                    Nip = "0000000000",
                },
                new BusinessActivity
                {
                    Nip = "111-11-11-111",
                },
                new BusinessActivity
                {
                    Nip = "222-222-22-22",
                },
                new BusinessActivity
                {
                    Nip = "PL3333333333",
                },
            }.AsQueryable();
        }

        [Fact]
        public void NipPatternGenerator_Works_ForNipWithDashesForBusinesses()
        {
            // Arrange
            var nip = "1111111111";
            var generator = new NipPatternGenerator();
            var pattern = generator.GenerateNipPatternWithDashesForBusinesses(nip);
            var query = from b in _businessActivities
                        where EF.Functions.Like(b.Nip, pattern)
                        select b;

            // Act
            var result = query.FirstOrDefault();

            // Assert
            result.Should().NotBeNull();
        }

        [Fact]
        public void NipPatternGenerator_Works_ForNipWithDashesForIndividuals()
        {
            // Arrange
            var nip = "2222222222";
            var generator = new NipPatternGenerator();
            var pattern = generator.GenerateNipPatternWithDashesForIndividuals(nip);
            var query = from b in _businessActivities
                        where EF.Functions.Like(b.Nip, pattern)
                        select b;

            // Act
            var result = query.FirstOrDefault();

            // Assert
            result.Should().NotBeNull();
        }

        [Fact]
        public void NipPatternGenerator_Works_ForNipWithLeadingCountryLetters()
        {
            // Arrange
            var nip = "3333333333";
            var generator = new NipPatternGenerator();
            var pattern = generator.GenerateNipPatternWithLeadingCountryLetters(nip);
            var query = from b in _businessActivities
                        where EF.Functions.Like(b.Nip, pattern)
                        select b;

            // Act
            var result = query.FirstOrDefault();

            // Assert
            result.Should().NotBeNull();
        }
    }
}
