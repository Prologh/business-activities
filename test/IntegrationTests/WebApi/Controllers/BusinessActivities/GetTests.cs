﻿using BusinessActivities.Models;
using FluentAssertions;
using Newtonsoft.Json;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace BusinessActivities.IntegrationTests.WebApi.Controllers.BusinessActivities
{
    public class GetTests : BusinessActivitiesWebApiFixtureBase
    {
        public GetTests(BusinessActivitiesWebApiFactory factory) : base(factory)
        {

        }

        [Fact]
        public async Task Get_ReturnsBadRequest_ForId_TooLong()
        {
            // Arrange
            var id = "1234567890432423";
            var uri = $"api/BusinessActivities/{id}";
            var client = Factory.CreateHttpsClient();

            // Act
            var response = await client.GetAsync(uri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task Get_ReturnsBadRequest_ForId_TooShort()
        {
            // Arrange
            var id = "123";
            var uri = $"api/BusinessActivities/{id}";
            var client = Factory.CreateHttpsClient();

            // Act
            var response = await client.GetAsync(uri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task Get_ReturnsBadRequest_ForNip_BadlyFormatted()
        {
            // Arrange
            var id = "72-51-801-126";
            var uri = $"api/BusinessActivities/{id}";
            var client = Factory.CreateHttpsClient();

            // Act
            var response = await client.GetAsync(uri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task Get_ReturnsBadRequest_ForNip_WithNonPolishCountryLetters()
        {
            // Arrange
            var id = "FR7251801126";
            var uri = $"api/BusinessActivities/{id}";
            var client = Factory.CreateHttpsClient();

            // Act
            var response = await client.GetAsync(uri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task Get_ReturnsMatchingEntity_WhenFound_ByKrs()
        {
            // Arrange
            var krs  = "0000045146";
            var uri = $"api/BusinessActivities/{krs}";
            var client = Factory.CreateHttpsClient();

            // Act
            var response = await client.GetAsync(uri);
            var json = await response.Content.ReadAsStringAsync();
            var entity = JsonConvert.DeserializeObject<BusinessActivity>(json);

            // Assert
            response.IsSuccessStatusCode.Should().BeTrue();
            entity.Krs.Should().Be(krs);
        }

        [Fact]
        public async Task Get_ReturnsMatchingEntity_WhenFound_ByNip_PleanDigits()
        {
            // Arrange
            var nip = "7251801126";
            var uri = $"api/BusinessActivities/{nip}";
            var client = Factory.CreateHttpsClient();

            // Act
            var response = await client.GetAsync(uri);
            var json = await response.Content.ReadAsStringAsync();
            var entity = JsonConvert.DeserializeObject<BusinessActivity>(json);

            // Assert
            response.IsSuccessStatusCode.Should().BeTrue();
            entity.Nip.Should().NotBeNull();
        }

        [Fact]
        public async Task Get_ReturnsMatchingEntity_WhenFound_ByNip_LeadingCountryLetters()
        {
            // Arrange
            var nip = "PL7251801126";
            var uri = $"api/BusinessActivities/{nip}";
            var client = Factory.CreateHttpsClient();

            // Act
            var response = await client.GetAsync(uri);
            var json = await response.Content.ReadAsStringAsync();
            var entity = JsonConvert.DeserializeObject<BusinessActivity>(json);

            // Assert
            response.IsSuccessStatusCode.Should().BeTrue();
            entity.Nip.Should().NotBeNull();
        }

        [Fact]
        public async Task Get_ReturnsMatchingEntity_WhenFound_ByNip_WithDashesForBusinesses()
        {
            // Arrange
            var nip = "725-18-01-126";
            var uri = $"api/BusinessActivities/{nip}";
            var client = Factory.CreateHttpsClient();

            // Act
            var response = await client.GetAsync(uri);
            var json = await response.Content.ReadAsStringAsync();
            var entity = JsonConvert.DeserializeObject<BusinessActivity>(json);

            // Assert
            response.IsSuccessStatusCode.Should().BeTrue();
            entity.Nip.Should().NotBeNull();
        }

        [Fact]
        public async Task Get_ReturnsMatchingEntity_WhenFound_ByNip_WithDashesForIndividuals()
        {
            // Arrange
            var nip = "725-180-11-26";
            var uri = $"api/BusinessActivities/{nip}";
            var client = Factory.CreateHttpsClient();

            // Act
            var response = await client.GetAsync(uri);
            var json = await response.Content.ReadAsStringAsync();
            var entity = JsonConvert.DeserializeObject<BusinessActivity>(json);

            // Assert
            response.IsSuccessStatusCode.Should().BeTrue();
            entity.Nip.Should().NotBeNull();
        }

        [Fact]
        public async Task Get_ReturnsMatchingEntity_WhenFound_ByRegon()
        {
            // Arrange
            var regon = "472836141";
            var uri = $"api/BusinessActivities/{regon}";
            var client = Factory.CreateHttpsClient();

            // Act
            var response = await client.GetAsync(uri);
            var json = await response.Content.ReadAsStringAsync();
            var entity = JsonConvert.DeserializeObject<BusinessActivity>(json);

            // Assert
            response.IsSuccessStatusCode.Should().BeTrue();
            entity.Regon.Should().Be(regon);
        }

        [Fact]
        public async Task Get_ReturnsNotFound_WhenNoMatchingEntityIsFound()
        {
            // Arrange
            var id = "1234567890";
            var uri = $"api/BusinessActivities/{id}";
            var client = Factory.CreateHttpsClient();

            // Act
            var response = await client.GetAsync(uri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }
    }
}
