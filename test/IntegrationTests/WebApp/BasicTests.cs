﻿using FluentAssertions;
using System;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace BusinessActivities.IntegrationTests.WebApp
{
    public class BasicTests : BusinessActivitiesWebAppFixtureBase
    {
        public BasicTests(BusinessActivitiesWebAppFactory factory) : base(factory)
        {

        }

        [Fact]
        public async Task Get_EndpointsRedirectHttpRequests()
        {
            // Arrange
            var uri = "";
            var client = Factory.CreateDefaultClient();

            // Act
            var response = await client.GetAsync(uri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.TemporaryRedirect);
            response.Headers.Location.Scheme.Should().Be(Uri.UriSchemeHttps);
        }

        [Fact]
        public async Task Get_EndpointsReturnCorrectCharset()
        {
            // Arrange
            var uri = "";
            var client = Factory.CreateHttpsClient();

            // Act
            var response = await client.GetAsync(uri);

            // Assert
            response.Content.Headers.ContentType.CharSet.Should().Be("utf-8");
        }

        [Fact]
        public async Task Get_EndpointsReturnCorrectContentType()
        {
            // Arrange
            var uri = "";
            var client = Factory.CreateHttpsClient();

            // Act
            var response = await client.GetAsync(uri);

            // Assert
            response.Content.Headers.ContentType.MediaType.Should().Be("text/html");
        }

        [Fact]
        public async Task Get_EndpointsReturnSuccessCode()
        {
            // Arrange
            var uri = "";
            var client = Factory.CreateHttpsClient();

            // Act
            var response = await client.GetAsync(uri);

            // Assert
            response.IsSuccessStatusCode.Should().BeTrue();
        }
    }
}
