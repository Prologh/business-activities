﻿using Xunit;

namespace BusinessActivities.IntegrationTests.WebApp
{
    /// <summary>
    /// Provides base fixture for Business Activities Web App integration tests.
    /// </summary>
    public abstract class BusinessActivitiesWebAppFixtureBase : IClassFixture<BusinessActivitiesWebAppFactory>
    {
        /// <summary>
        /// Creates a new instance of <see cref=""/>
        /// class.
        /// </summary>
        /// <param name="factory">
        /// The Web API factory.
        /// </param>
        public BusinessActivitiesWebAppFixtureBase(BusinessActivitiesWebAppFactory factory)
        {
            Factory = factory;
        }

        /// <summary>
        /// Gets the web application factory.
        /// </summary>
        protected BusinessActivitiesWebAppFactory Factory { get; }
    }
}
