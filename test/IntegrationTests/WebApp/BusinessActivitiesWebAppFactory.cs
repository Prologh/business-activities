﻿using BusinessActivities.WebApp;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using System;
using System.Net.Http;

namespace BusinessActivities.IntegrationTests.WebApp
{
    /// <summary>
    /// Factory for bootstrapping Business Activities Web App in memory
    /// for functional end to end tests.
    /// </summary>
    public class BusinessActivitiesWebAppFactory : WebApplicationFactory<Startup>
    {
        /// <summary>
        /// Creates a new instance of an <see cref="HttpClient"/> that
        /// can be used to send <see cref="HttpRequestMessage"/> to the server.
        /// The base address of the <see cref="HttpClient"/> instance
        /// will be set to https://localhost:5001.
        /// </summary>
        /// <returns>
        /// An instance of <see cref="HttpClient"/>.
        /// </returns>
        public virtual HttpClient CreateHttpsClient()
        {
            return CreateClient(new WebApplicationFactoryClientOptions
            {
                BaseAddress = new Uri("https://localhost:5001"),
            });
        }

        /// <summary>
        /// Creates a new instance of an <see cref="HttpClient"/> that
        /// can be used to send <see cref="HttpRequestMessage"/> to the server.
        /// The base address of the <see cref="HttpClient"/> instance
        /// will be set to https://localhost:5001.
        /// </summary>
        /// <param name="options">
        /// Options used for creation <see cref="HttpClient"/>.
        /// </param>
        /// <returns>
        /// An instance of <see cref="HttpClient"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="options"/> is <see langword="null"/>.
        /// </exception>
        public virtual HttpClient CreateHttpsClient(WebApplicationFactoryClientOptions options)
        {
            if (options == null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            options.BaseAddress = new Uri("https://localhost:5001");

            return CreateClient(options);
        }

        /// <summary>
        /// Creates a new instance of an <see cref="HttpClient"/> that
        /// can be used to send <see cref="HttpRequestMessage"/> to the server.
        /// The base address of the <see cref="HttpClient"/> instance
        /// will be set to https://localhost:5001.
        /// </summary>
        /// <param name="options">
        /// Action for configuring <see cref="WebApplicationFactoryClientOptions"/>
        /// used for creation <see cref="HttpClient"/>.
        /// </param>
        /// <returns>
        /// An instance of <see cref="HttpClient"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="action"/> is <see langword="null"/>.
        /// </exception>
        public virtual HttpClient CreateHttpsClient(Action<WebApplicationFactoryClientOptions> action)
        {
            if (action == null)
            {
                throw new ArgumentNullException(nameof(action));
            }

            var defaultOptions = ClientOptions;
            defaultOptions.BaseAddress = new Uri("https://localhost:5001");
            action(defaultOptions);

            return CreateClient(defaultOptions);
        }

        /// <summary>
        /// Gives a fixture an opportunity to configure the application
        /// before it gets built.
        /// </summary>
        /// <param name="builder">
        /// The <see cref="IWebHostBuilder"/> for the application.
        /// </param>
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {

        }
    }
}
