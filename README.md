﻿# Business Activities

README / 24 June 2019

-----

## Introduction

Business Activities is a simple RESTful API and web application for searching for business activities.

## Technical information

Name | Value
--- | ---
Main programming language | C#
Framework used | ASP.NET Core 2.2

#### Back-end libraries used (NuGet packages)

Name | Version
--- | ---
FluentAssertions | [![Nuget](https://img.shields.io/nuget/v/FluentAssertions.svg)](https://www.nuget.org/packages/FluentAssertions/)
FluentValidation.AspNetCore | [![Nuget](https://img.shields.io/nuget/v/FluentValidation.AspNetCore.svg)](https://www.nuget.org/packages/FluentValidation.AspNetCore/)
Microsoft.AspNetCore.Mvc.Testing | [![Nuget](https://img.shields.io/nuget/v/Microsoft.AspNetCore.Mvc.Testing.svg)](https://www.nuget.org/packages/Microsoft.AspNetCore.Mvc.Testing/)
Swashbuckle.AspNetCore | [![Nuget](https://img.shields.io/nuget/v/Swashbuckle.AspNetCore.svg)](https://www.nuget.org/packages/Swashbuckle.AspNetCore/)
xunit | [![Nuget](https://img.shields.io/nuget/v/xunit.svg)](https://www.nuget.org/packages/xunit/)

#### Front-end libraries used (npm packages)

Name | Version
--- | ---
bootstrap | [![npm](https://img.shields.io/npm/v/bootstrap.svg)](https://www.npmjs.com/package/bootstrap)
@fortawesome/fontawesome-free | [![npm](https://img.shields.io/npm/v/@fortawesome/fontawesome-free.svg)](https://www.npmjs.com/package/@fortawesome/fontawesome-free)
gulp | [![npm](https://img.shields.io/npm/v/gulp.svg)](https://www.npmjs.com/package/gulp)
gulp-autoprefixer | [![npm](https://img.shields.io/npm/v/gulp-autoprefixer.svg)](https://www.npmjs.com/package/gulp-autoprefixer)
gulp-cli | [![npm](https://img.shields.io/npm/v/gulp-cli.svg)](https://www.npmjs.com/package/gulp-cli)
gulp-cssmin | [![npm](https://img.shields.io/npm/v/gulp-cssmin.svg)](https://www.npmjs.com/package/gulp-cssmin)
gulp-rename | [![npm](https://img.shields.io/npm/v/gulp-rename.svg)](https://www.npmjs.com/package/gulp-rename)
gulp-sass | [![npm](https://img.shields.io/npm/v/gulp-sass.svg)](https://www.npmjs.com/package/gulp-sass)
gulp-terser | [![npm](https://img.shields.io/npm/v/gulp-terser.svg)](https://www.npmjs.com/package/gulp-terser)
jquery | [![npm](https://img.shields.io/npm/v/jquery.svg)](https://www.npmjs.com/package/jquery)
jquery-validation | [![npm](https://img.shields.io/npm/v/jquery-validation.svg)](https://www.npmjs.com/package/jquery-validation)
jquery-validation-unobtrusive | [![npm](https://img.shields.io/npm/v/jquery-validation-unobtrusive.svg)](https://www.npmjs.com/package/jquery-validation-unobtrusive)
node-sass | [![npm](https://img.shields.io/npm/v/node-sass.svg)](https://www.npmjs.com/package/node-sass)
rimraf | [![npm](https://img.shields.io/npm/v/rimraf.svg)](https://www.npmjs.com/package/rimraf)

## Prerequisites

To **build** this project you'll need:

 - Microsoft Visual Studio 2017 (or later) - [download](https://visualstudio.microsoft.com/)
 - Microsoft .NET Core 2.2 SDK (usually comes with Visual Studio) - [download](https://dotnet.microsoft.com/download/dotnet-core/2.2)
 - NodeJS (usually comes with Visual Studio) - [download](https://nodejs.org/en/)

To **run** this project you'll need:

 - Microsoft .NET Core 2.2 Runtime - [download](https://dotnet.microsoft.com/download/dotnet-core/2.2)
 - NodeJS - [download](https://nodejs.org/en/)

**Note:** if you are downloading .NET Core SDK, check if it supports your version of Visual Studio. For example SDK 2.2.202 supports only Visual Studio 2019.

## Getting started

This project is divided into two services:

Project | HTTP | HTTPS
--- | --- | ---
Web API | http://localhost:44300/ | https://localhost:44301/
Web App | http://localhost:80/ | https://localhost:443/

Web Application consumes Web API, so to run it you'll have to launch the Web API first. 

There are two ways to do this. You can either:

 - stick with Visual Studio and let it hassle for you;
 - execute commands on your own with CLI.

### Visual Studio way

 1. Change default startup project from list to `WebApi`;
 2. Build the solution;
 2. Hit `Ctrl` + `F5` to launch Web API without debugging;
 3. In `WebApp` project click with right mouse button on file `package.json` and select `Restore Packages`;
 4. Wait for npm packages to restore;
 5. Click with right mouse button on file `gulp.js` and select `Task Runner Explorer`;
 6. Wait few seconds as Visual Studio should automatically assign gulp watch task to Task Runner. If needed click the blue "Refresh" arrow in left top corner of the Task Runner window. When it succeeds, you should be able to see a list of gulp tasks.
 7. Build the `WebApp` project;
 8. Wait for gulp tasks to complete; 
 9. Change default startup project from list to `WebApp`;
 10. Hit `Ctrl` + `F5` to launch web application without debugging.

### CLI way

Proceed to `WebApi` directory and then execute:

```
dotnet run
```

Don't worry about the warnings, it's because the Web API project has enabled option to generate XML documentation file needed for Swagger. Thus, it lists sections without proper XML documentation e.g. auto-generated migrations classes.

Okay, that should not only launch the API, but automatically create the database and seed it with initial data. You can check if the API is running for yourself. You should be able to see familiar Swagger UI.

Now, let's take care of web app. But, we have to prepare first. Let's install frontend packages. Proceed to the `WebApp` directory and execute:

```
npm install
```

Good. Now, let gulp do the frontend magic (SCSS compilation, minification and other). Execute:

```
gulp
```

Finally, run the web application with:

```
dotnet run
```

Great! Now, you can check if the web app is running and start searching for business activities! 
 
## Migrations

All migrations are stored at `src\WebApi\Migrations`. All kind of migration-related operations should be executed in direct context of  `WebApi` project.

## Request Logging

Web API logs incoming requests to database, in the `RequestLog` table. Request logging is performed by `RequestLoggingMiddleware`. `RequestLoggingMiddleware` options are automatically applied when changed in `appsettings.json`. Request logging can be configured with following options:

##### LogLevel

 Indicates the level of request logs that should be saved. Available options are:

 - `None` - Do not log request. This option turns off requests logging.
 - `BusinessActivityRequests` - Log only requests related to business activity.
 - `All` - Log every request.

Defaults to `None`.

 ##### LogOnlySuccessfulRequests

A `bool` value indicating whether logger should log only successful requests. Setting this option to `true` indicates that all unsuccessful  requests (e.g. with response code 400, 404) will not be logged. 

Defaults to `false`.

## Tests

This project contain two types of tests:

 - integration tests - divided into those for Web API and those for Web App. Integration tests simulating HTTP requests use In Memory database.
 - unit tests

## Swagger

The Web API supports Swagger with its UI available at root address of Web API.

## License

Licensed under MIT. Read full license [here](https://gitlab.com/Prologh/business-activities/blob/master/LICENSE).

## Credits

**Piotr Wosiek** | [GitLab](https://gitlab.com/Prologh) | [GitHub](https://github.com/Prologh)

### Acknowledgements

Project icon based on one made by [Freepik](https://www.freepik.com/) from [www.flaticon.com](https://www.flaticon.com/).
