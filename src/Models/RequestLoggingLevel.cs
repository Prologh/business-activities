﻿namespace BusinessActivities.Models
{
    /// <summary>
    /// Indicates logging level for request logging middleware.
    /// </summary>
    public enum RequestLoggingLevel
    {
        /// <summary>
        /// Do not log request. This option turns off requests logging.
        /// </summary>
        None = 0,

        /// <summary>
        /// Log only requests related to business activity.
        /// </summary>
        BusinessActivityRequests,

        /// <summary>
        /// Log every request.
        /// </summary>
        All
    }
}
