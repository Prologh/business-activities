﻿using Newtonsoft.Json;

namespace BusinessActivities.Models
{
    /// <summary>
    /// Represents business activity.
    /// </summary>
    public class BusinessActivity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessActivity"/> class.
        /// </summary>
        public BusinessActivity()
        {

        }

        /// <summary>
        /// Gets or sets the city name.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        [JsonIgnore]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the National Court Register (KRS) number.
        /// <para/>
        /// KRS is a ten-digit number from public register, maintained
        /// by selected district courts and Polish Ministry of Justice.
        /// </summary>
        public string Krs { get; set; }

        /// <summary>
        /// Gets or sets the business name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Tax Identification Number (NIP).
        /// <para/>
        /// NIP is a ten-digit code used to identify taxpayers in Poland.
        /// </summary>
        public string Nip { get; set; }

        /// <summary>
        /// Gets or sets the postal code.
        /// </summary>
        public string PostalCode { get; set; }

        /// <summary>
        /// Gets or sets the Register of National Economy (REGON) number.
        /// <para/>
        /// REGON is a nine-digit number of subject from Register of National Economy.
        /// </summary>
        public string Regon { get; set; }

        /// <summary>
        /// Gets or sets the name of the street.
        /// </summary>
        public string Street { get; set; }

        /// <summary>
        /// Gets or sets the street number.
        /// </summary>
        public string StreetNumber { get; set; }
    }
}
