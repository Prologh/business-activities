﻿namespace BusinessActivities.Models
{
    /// <summary>
    /// Represents settings for <see cref="Middleware.RequestLoggingMiddleware"/>.
    /// </summary>
    public class RequestLoggingOptions
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RequestLoggingOptions"/> class.
        /// </summary>
        public RequestLoggingOptions()
        {

        }

        /// <summary>
        /// Indicates the level of request logs that should be saved.
        /// <para/>
        /// Defaults to <see cref="RequestLoggingLevel.None"/>.
        /// </summary>
        public RequestLoggingLevel LogLevel { get; set; }

        /// <summary>
        /// Gets or sets a <see cref="bool"/> value indicating whether
        /// logger should log only successful requests. Setting this option
        /// to <see langword="true"/> indicates that all unsuccessful
        /// requests (e.g. with response code 400, 404) will not be logged.
        /// <para/>
        /// Defaults to <see langword="false"/>.
        /// </summary>
        public bool LogOnlySuccessfulRequests { get; set; }
    }
}
