﻿using BusinessActivities.Models;
using Microsoft.EntityFrameworkCore;

namespace BusinessActivities.WebApi.Data
{
    /// <summary>
    /// Represents a session with the database and can be used to query
    /// and save instances of entities.
    /// </summary>
    public class ApiDbContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApiDbContext"/>
        /// class using the specified options.
        /// </summary>
        /// <param name="options">
        /// The options for this context.
        /// </param>
        public ApiDbContext(DbContextOptions<ApiDbContext> options) : base(options)
        {

        }

        /// <summary>
        /// Gets or sets <see cref="DbSet{TEntity}"/> of BusinessActivitiess.
        /// </summary>
        public DbSet<BusinessActivity> BusinessActivities { get; set; }

        /// <summary>
        /// Configures the model that was discovered by convention from
        /// the entity types exposed in <see cref="DbSet{TEntity}"/> properties
        /// on derived context.
        /// </summary>
        /// <param name="builder">
        /// The builder being used to construct the model for this context.
        /// </param>
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<BusinessActivity>()
                .ToTable(nameof(BusinessActivity));

            builder.Entity<RequestLog>()
                .ToTable(nameof(RequestLog));
        }
    }
}
