﻿using BusinessActivities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace BusinessActivities.WebApi.Data
{
    /// <summary>
    /// Provides means of database initialization.
    /// </summary>
    public static class DatabaseInitializer
    {
        #region seed data
        private static readonly BusinessActivity[] Businesses = new BusinessActivity[]
        {
            new BusinessActivity
            {
                Krs = "0000045146",
                Nip = "7251801126",
                Regon = "472836141",
                Name = "Muzeum Broni Pancernej",
                City = "Poznań",
                PostalCode = "60-300",
                Street = "Grunwaldzka",
                StreetNumber = "21",
            },
            new BusinessActivity
            {
                Krs = "0000630893",
                Nip = "584-275-19-79",
                Name = "Fundacja Psia Micha",
                City = "Wrocław",
                PostalCode = "00-710",
                Street = "Młynarska",
                StreetNumber = "33",
            },
            new BusinessActivity
            {
                Nip = "583-33-54-641",
                Name = "ABC sp. z o.o.",
                City = "Gdańsk",
                PostalCode = "80-008",
                Street = "Gdyńska",
                StreetNumber = "131",
            },
            new BusinessActivity
            {
                Krs = "0000684354",
                Regon = "367632804",
                Name = "Prosto Sp. z o.o.",
                City = "Warszawa",
                PostalCode = "00-006",
                Street = "Mickiewicza",
                StreetNumber = "234/7",
            },
            new BusinessActivity
            {
                Nip = "PL8421778416",
                Regon = "380696984",
                Name = "Przedsiębiorstwo Handlowo Usługowe Galimatias",
                City = "Ryków Mały",
                PostalCode = "12-300",
                StreetNumber = "3",
            },
        };
        #endregion

        /// <summary>
        /// Creates the database if it does not exists and seeds it with
        /// initial data.
        /// </summary>
        /// <param name="dbContext">
        /// The database context for Web API.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="dbContext"/> is <see langword="null"/>.
        /// </exception>
        public static void Initialize(ApiDbContext dbContext)
        {
            if (dbContext == null)
            {
                throw new ArgumentNullException(nameof(dbContext));
            }

            dbContext.Database.EnsureCreated();

            if (!dbContext.BusinessActivities.AsNoTracking().Any())
            {
                dbContext.AddRange(Businesses);
                dbContext.SaveChanges();
            }
        }
    }
}
