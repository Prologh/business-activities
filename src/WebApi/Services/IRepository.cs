﻿using System.Threading.Tasks;

namespace BusinessActivities.WebApi.Services
{
    /// <summary>
    /// Provides management functionalites over defined type of entity.
    /// </summary>
    /// <typeparam name="TEntity">
    /// The type of entity being operated on.
    /// </typeparam>
    public interface IRepository<TEntity>
    {
        /// <summary>
        /// Finds an entity matching provider identifier.
        /// </summary>
        /// <param name="id">
        /// The entity identifier.
        /// </param>
        /// <returns>
        /// An instance of <typeparamref name="TEntity"/> or <see langword="null"/>
        /// if not matching entity was found.
        /// </returns>
        TEntity Find(object id);

        /// <summary>
        /// Asynchronously finds an entity matching provider identifier.
        /// </summary>
        /// <param name="id">
        /// The entity identifier.
        /// </param>
        /// <returns>
        /// An instance of <typeparamref name="TEntity"/> or <see langword="null"/>
        /// if not matching entity was found.
        /// </returns>
        Task<TEntity> FindAsync(object id);
    }
}
