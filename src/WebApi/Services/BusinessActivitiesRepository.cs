﻿using BusinessActivities.Models;
using BusinessActivities.WebApi.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BusinessActivities.WebApi.Services
{
    /// <summary>
    /// Provides management functionalites over <see cref="BusinessActivity"/>s.
    /// </summary>
    public class BusinessActivitiesRepository : IRepository<BusinessActivity>
    {
        private readonly ApiDbContext _dbContext;
        private readonly INipPatternGenerator _nipPatternGenerator;

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessActivitiesRepository"/>
        /// class.
        /// </summary>
        public BusinessActivitiesRepository(ApiDbContext dbContext, INipPatternGenerator nipPatternGenerator)
        {
            _dbContext = dbContext;
            _nipPatternGenerator = nipPatternGenerator;
        }

        /// <summary>
        /// Finds an entity matching provider identifier.
        /// </summary>
        /// <param name="id">
        /// The entity identifier.
        /// </param>
        /// <returns>
        /// An instance of <see cref="BusinessActivity"/> or <see langword="null"/>
        /// if not matching entity was found.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="id"/> is <see langword="null"/>.
        /// </exception>
        public BusinessActivity Find(object id)
        {
            if (id == null)
            {
                throw new ArgumentNullException(nameof(id));
            }

            var plainDigitsId = GetOnlyDigitsString(id.ToString());
            var query = plainDigitsId.Length == 10
                ? GetBusinessesQueryWithNipPatterns(plainDigitsId)
                : GetBusinessesQuery(plainDigitsId);

            return query.AsNoTracking().FirstOrDefault();
        }

        /// <summary>
        /// Asynchronously finds an entity matching provider identifier.
        /// </summary>
        /// <param name="id">
        /// The entity identifier.
        /// </param>
        /// <returns>
        /// An instance of <see cref="BusinessActivity"/> or <see langword="null"/>
        /// if not matching entity was found.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="id"/> is <see langword="null"/>.
        /// </exception>
        public async Task<BusinessActivity> FindAsync(object id)
        {
            if (id == null)
            {
                throw new ArgumentNullException(nameof(id));
            }

            var plainDigitsId = GetOnlyDigitsString(id.ToString());
            var query = plainDigitsId.Length == 10
                ? GetBusinessesQueryWithNipPatterns(plainDigitsId)
                : GetBusinessesQuery(plainDigitsId);

            return await query.AsNoTracking().FirstOrDefaultAsync();
        }

        private string GetOnlyDigitsString(string id)
        {
            return string.Join(string.Empty, id.Where(sign => char.IsDigit(sign)).ToArray());
        }

        private IQueryable<BusinessActivity> GetBusinessesQuery(string id)
        {
            return from b in _dbContext.BusinessActivities
                   where b.Krs == id
                       || b.Nip == id
                       || b.Regon == id
                   select b;
        }

        private IQueryable<BusinessActivity> GetBusinessesQueryWithNipPatterns(string id)
        {
            return from b in _dbContext.BusinessActivities
                   where b.Krs == id
                       || b.Nip == id
                       || EF.Functions.Like(b.Nip, _nipPatternGenerator.GenerateNipPatternWithDashesForBusinesses(id))
                       || EF.Functions.Like(b.Nip, _nipPatternGenerator.GenerateNipPatternWithDashesForIndividuals(id))
                       || EF.Functions.Like(b.Nip, _nipPatternGenerator.GenerateNipPatternWithLeadingCountryLetters(id))
                       || b.Regon == id
                   select b;
        }
    }
}
