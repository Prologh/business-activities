﻿namespace BusinessActivities.WebApi.Services
{
    /// <summary>
    /// Provides means of genereting known patters for NIP numbers.
    /// </summary>
    public interface INipPatternGenerator
    {
        /// <summary>
        /// E.g. 333-33-33-333
        /// </summary>
        /// <param name="nip"></param>
        /// <returns></returns>
        string GenerateNipPatternWithDashesForIndividuals(string nip);

        /// <summary>
        /// E.g. 222-222-22-22
        /// </summary>
        /// <param name="nip"></param>
        /// <returns></returns>
        string GenerateNipPatternWithDashesForBusinesses(string nip);

        /// <summary>
        /// E.g. PL1111111111
        /// </summary>
        /// <param name="nip"></param>
        /// <returns></returns>
        string GenerateNipPatternWithLeadingCountryLetters(string nip);
    }
}
