﻿using System;
using System.Linq;

namespace BusinessActivities.WebApi.Services
{
    public class NipPatternGenerator : INipPatternGenerator
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NipPatternGenerator"/>
        /// class.
        /// </summary>
        public NipPatternGenerator()
        {

        }

        // 333-33-33-333 - numbers with dashes in between (businesses)
        public string GenerateNipPatternWithDashesForBusinesses(string nip)
        {
            Validate(nip);

            return $"{nip.Substring(0, 3)}-{nip.Substring(3, 2)}-{nip.Substring(5, 2)}-{nip.Substring(7, 3)}";
        }

        // 222-222-22-22 - numbers with dashes in between (individuals)
        public string GenerateNipPatternWithDashesForIndividuals(string nip)
        {
            Validate(nip);

            return $"{nip.Substring(0, 3)}-{nip.Substring(3, 3)}-{nip.Substring(6, 2)}-{nip.Substring(8, 2)}";
        }

        // PL1111111111 - plain numbers but with leading country letters
        public string GenerateNipPatternWithLeadingCountryLetters(string nip)
        {
            Validate(nip);

            return $"PL{nip}";
        }

        private void Validate(string nip)
        {
            if (string.IsNullOrWhiteSpace(nip))
            {
                throw new ArgumentException(
                    $"{nameof(nip)} cannot be null, empty " +
                    $"or contain only whitespace characters.",
                    nameof(nip));
            }

            if (nip.Length != 10)
            {
                throw new ArgumentException(
                    $"{nameof(nip)} must be a string of 10 digits.",
                    nameof(nip));
            }

            if (nip.Any(c => !char.IsDigit(c)))
            {
                throw new ArgumentException(
                    $"{nameof(nip)} must consists of only digits from 0 to 9.",
                    nameof(nip));
            }
        }
    }
}
