﻿using BusinessActivities.WebApi.Filters;
using BusinessActivities.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;

namespace BusinessActivities.WebApi.Services
{
    /// <summary>
    /// Provides means of formatting request logs.
    /// </summary>
    public class RequestLogFormatter : IRequestLogFormatter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RequestLogFormatter"/>
        /// class.
        /// </summary>
        public RequestLogFormatter()
        {

        }

        /// <summary>
        /// Creates a single request log from provided data.
        /// </summary>
        /// <param name="httpContext">
        /// The <see cref="HttpContext"/> providing information about HTTP request
        /// and response.
        /// </param>
        /// <param name="created">
        /// Date and time when the request started.
        /// </param>
        /// <param name="duration">
        /// The duration of request execution.
        /// </param>
        /// <returns>
        /// An instance of <see cref="RequestLog"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="httpContext"/> is <see langword="null"/>.
        /// </exception>
        public RequestLog FormatRequestLog(HttpContext httpContext, DateTime created, TimeSpan duration)
        {
            if (httpContext == null)
            {
                throw new ArgumentNullException(nameof(httpContext));
            }

            return new RequestLog
            {
                BusinessActivityId = FormatBusinessActivityId(httpContext),
                Created = created,
                Duration = duration.TotalMilliseconds,
                HttpMethod = httpContext.Request.Method,
                HttpStatusCode = httpContext.Response.StatusCode,
                IPAddress = FormatIPAddress(httpContext.Connection.RemoteIpAddress),
                Query = FormatStringValues(httpContext.Request.Query),
                RequestHeaders = FormatStringValues(httpContext.Request.Headers),
                ResponseHeaders = FormatStringValues(httpContext.Response.Headers),
                Route = httpContext.Request.Path,
            };
        }

        private string FormatBusinessActivityId(HttpContext httpContext)
        {
            return httpContext.Items.TryGetValue(RequestLoggingAttribute.BusinessActivityIdKey, out object value)
                ? value?.ToString()
                : null;
        }

        private string FormatIPAddress(IPAddress ip)
        {
            if (ip == null)
            {
                return null;
            }
            else
            {
                // Map only if not IPv4.
                return ip.AddressFamily == AddressFamily.InterNetwork
                    ? ip.ToString()
                    : ip.MapToIPv4().ToString();
            }
        }

        private string FormatStringValues(IEnumerable<KeyValuePair<string, StringValues>> stringValues)
        {
            return string.Join(Environment.NewLine, stringValues.Select(h => $"{h.Key}={string.Join(",", h.Value)}"));
        }
    }
}
