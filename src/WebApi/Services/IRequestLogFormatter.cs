﻿using BusinessActivities.Models;
using Microsoft.AspNetCore.Http;
using System;

namespace BusinessActivities.WebApi.Services
{
    /// <summary>
    /// Provides means of formatting request logs.
    /// </summary>
    public interface IRequestLogFormatter
    {
        /// <summary>
        /// Creates a single request log from provided data.
        /// </summary>
        /// <param name="httpContext">
        /// The <see cref="HttpContext"/> providing information about HTTP request
        /// and response.
        /// </param>
        /// <param name="created">
        /// Date and time when the request started.
        /// </param>
        /// <param name="duration">
        /// The duration of request execution.
        /// </param>
        /// <returns>
        /// An instance of <see cref="RequestLog"/>.
        /// </returns>
        RequestLog FormatRequestLog(HttpContext httpContext, DateTime created, TimeSpan duration);
    }
}
