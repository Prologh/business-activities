﻿using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace BusinessActivities.Extensions.DependencyInjection
{
    /// <summary>
    /// Provides extension methods for adding Swagger generator to
    /// <see cref="IServiceCollection"/>.
    /// </summary>
    public static class SwaggerGeneratorServiceCollectionExtensions
    {
        /// <summary>
        /// Adds Swagger generator to the <see cref="IServiceCollection"/>
        /// with default options registering one main Swagger document.
        /// </summary>
        /// <param name="services">
        /// Current <see cref="IServiceCollection"/> instance.
        /// </param>
        /// <returns>
        /// An instance of <see cref="IServiceCollection"/> with added
        /// Swagger generator so additional calls can be chained.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="services"/> is <see langword="null"/>.
        /// </exception>
        public static IServiceCollection AddSwaggerGenWithDefaultOptions(this IServiceCollection services)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }

            return services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Business Activities API",
                    Description = "A simple RESTful API managing business activities.",
                    Contact = new Contact
                    {
                        Name = "Piotr Wosiek",
                        Email = "piotr.m.wosiek@gmail.com",
                        Url = "http://wosiek.azurewebsites.net"
                    },
                    License = new License
                    {
                        Name = "Licensed under MIT",
                        Url = "https://gitlab.com/Prologh/business-activities/blob/master/LICENSE"
                    }
                });

                // Set the comments path for the Swagger JSON and UI.
                var xmlFiles = Directory.GetFiles(AppContext.BaseDirectory, "BusinessActivities*.xml", SearchOption.TopDirectoryOnly).ToList();
                xmlFiles.ForEach(xmlFile => options.IncludeXmlComments(xmlFile));
            });
        }
    }
}
