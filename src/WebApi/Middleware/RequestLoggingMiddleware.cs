﻿using BusinessActivities.WebApi.Data;
using BusinessActivities.WebApi.Filters;
using BusinessActivities.Models;
using BusinessActivities.WebApi.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace BusinessActivities.WebApi.Middleware
{
    /// <summary>
    /// Saves logs based on incoming HTTP requests.
    /// </summary>
    public class RequestLoggingMiddleware
    {
        private readonly RequestDelegate _next;

        /// <summary>
        /// Initializes a new instance of the <see cref="RequestLoggingMiddleware"/>
        /// class.
        /// </summary>
        /// <param name="next">
        /// A function that can process an HTTP request.
        /// </param>
        public RequestLoggingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        /// <summary>
        /// Asynchronously invokes the request logging middleware.
        /// </summary>
        public async Task Invoke(
            HttpContext context,
            ApiDbContext dbContext,
            IOptionsSnapshot<RequestLoggingOptions> options,
            ILogger<RequestLoggingMiddleware> logger,
            IRequestLogFormatter formatter)
        {
            var created = DateTime.Now;
            var stopwatch = Stopwatch.StartNew();

            await _next(context);

            stopwatch.Stop();

            if (options.Value.LogLevel == RequestLoggingLevel.None)
            {
                logger.LogInformation("Skipping request logging due to configuration.");

                return;
            }

            if (options.Value.LogLevel == RequestLoggingLevel.BusinessActivityRequests)
            {
                if (!context.Items.ContainsKey(RequestLoggingAttribute.BusinessActivityIdKey))
                {
                    logger.LogInformation("Skipping request logging due to configuration.");

                    return;
                }
            }

            if (options.Value.LogOnlySuccessfulRequests)
            {
                if (!IsSuccessStatusCode(context.Response.StatusCode))
                {
                    logger.LogInformation("Skipping request logging due to configuration.");

                    return;
                }
            }

            var requestLog = formatter.FormatRequestLog(context, created, stopwatch.Elapsed);
            logger.LogInformation("Saving request log.");

            dbContext.Add(requestLog);
            await dbContext.SaveChangesAsync();

            logger.LogInformation("Request log saved.");
        }

        private bool IsSuccessStatusCode(int statusCode)
        {
            return statusCode >= 200 && statusCode <= 299;
        }
    }
}
