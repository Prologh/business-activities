﻿using BusinessActivities.Extensions.DependencyInjection;
using BusinessActivities.Models;
using BusinessActivities.WebApi.Data;
using BusinessActivities.WebApi.Middleware;
using BusinessActivities.WebApi.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.IO;

namespace BusinessActivities.WebApi
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            Configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddDbContext<ApiDbContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
            });

            services.AddHttpsRedirection(options =>
            {
                options.HttpsPort = 44301;
            });

            services.AddSwaggerGenWithDefaultOptions();

            services.Configure<RequestLoggingOptions>(Configuration.GetSection("RequestsLogging"));

            services.AddScoped<IRepository<BusinessActivity>, BusinessActivitiesRepository>();
            services.AddScoped<INipPatternGenerator, NipPatternGenerator>();
            services.AddScoped<IRequestLogFormatter, RequestLogFormatter>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseMiddleware<RequestLoggingMiddleware>();
            app.UseHttpsRedirection();
            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "Business Activities API");
                options.RoutePrefix = string.Empty;
            });
            app.UseMvc();
        }
    }
}
