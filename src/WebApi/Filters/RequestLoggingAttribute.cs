﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace BusinessActivities.WebApi.Filters
{
    /// <summary>
    /// Indicates an action for which incoming requests should be logged by
    /// <see cref="Middleware.RequestLoggingMiddleware"/>.
    /// </summary>
    public class RequestLoggingAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// The key used for storing related business identifier in
        /// <see cref="Microsoft.AspNetCore.Http.HttpContext.Items"/> dictionary.
        /// </summary>
        public static string BusinessActivityIdKey => "BusinessActivityId";

        /// <summary>
        /// Initializes a new instance of the <see cref="RequestLoggingAttribute"/>
        /// class.
        /// </summary>
        public RequestLoggingAttribute()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RequestLoggingAttribute"/>
        /// class.
        /// </summary>
        /// <param name="routeKey">
        /// The name of a route key for incoming business activity identifier.
        /// </param>
        /// <exception cref="ArgumentException">
        /// <paramref name="routeKey"/> cannot be <see langword="null"/>,
        /// empty or contain only whitespace characters.
        /// </exception>
        public RequestLoggingAttribute(string routeKey)
        {
            if (string.IsNullOrWhiteSpace(routeKey))
            {
                throw new ArgumentException(
                    $"{nameof(routeKey)} cannot be null, empty " +
                    $"or contain only whitespace characters.",
                    nameof(routeKey));
            }

            RouteKey = routeKey;
        }

        /// <summary>
        /// Gets a key used to retrieve the business activity identifier
        /// from the route data.
        /// <para/>
        /// By default set to "id".
        /// </summary>
        public string RouteKey { get; } = "id";

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            if (!context.RouteData.Values.ContainsKey(RouteKey))
            {
                return;
            }

            var id = context.RouteData.Values[RouteKey];
            context.HttpContext.Items[BusinessActivityIdKey] = id;
        }
    }
}
