﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace BusinessActivities.WebApi.ComponentModel.DataAnnotations
{
    /// <summary>
    /// Specifies that a parameter, property or field should be validated
    /// against various forms of business activity identifier: KRS, NIP or REGON.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false, Inherited = true)]
    public class BusinessActivityIdAttribute : ValidationAttribute
    {
        private const string BusinessNipPatter = @"[\d]{3}-[\d]{3}-[\d]{2}-[\d]{2}";
        private const string IndividualNipPatter = @"[\d]{3}-[\d]{2}-[\d]{2}-[\d]{3}";

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessActivityIdAttribute"/>
        /// class.
        /// </summary>
        public BusinessActivityIdAttribute()
        {
            ErrorMessage = "Pole {0} ma niepoprawną wartość.";
            InvalidEuropeanFormatErrorMessage = "Pole {0} ma niepoprawny format. " +
                "NIP w formacie europejskim składa się z przedrostka 'PL' i ciągu 10 cyfr.";
            InvalidFormatErrorMessage = "Pole {0} ma niepoprawny format.";
            InvalidLengthErrorMessage = "Pole {0} ma niepoprawną długość. " +
                "Poprawny identyfikator ma długość 9, 10, 12 lub 13 znaków.";
            NonDigitsFoundErrorMessage = "Pole {0} może zwierać jedynie cyfry z wyjątkiem " +
                "na prefiks 'PL' bezpośrednio poprzedzający 10-cyfrowy NIP.";
        }

        /// <summary>
        /// Gets or sets the error message used for prompting invalid european format.
        /// </summary>
        public string InvalidEuropeanFormatErrorMessage { get; set; }

        /// <summary>
        /// Gets or sets the error message used for prompting invalid format.
        /// </summary>
        public string InvalidFormatErrorMessage { get; set; }

        /// <summary>
        /// Gets or sets the error message used for prompting invalid length.
        /// </summary>
        public string InvalidLengthErrorMessage { get; set; }

        /// <summary>
        /// Gets or sets the error message used for prompting about founding non-digits.
        /// </summary>
        public string NonDigitsFoundErrorMessage { get; set; }

        /// <summary>
        /// Gets a <see cref="bool"/> value that indicates whether
        /// the attribute requires validation context.
        /// </summary>
        public override bool RequiresValidationContext => true;

        /// <summary>
        /// Applies formatting to an error message, based on the data field
        /// where the error occurred.
        /// </summary>
        /// <param name="name">
        /// The name to include in the formatted message.
        /// </param>
        /// <returns>
        /// A <see cref="string "/> instance of the formatted error message.
        /// </returns>
        public override string FormatErrorMessage(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return ErrorMessageString != null
                ? string.Format(CultureInfo.CurrentCulture, ErrorMessageString, name)
                : string.Format(CultureInfo.CurrentCulture, ErrorMessage, name);
        }

        /// <summary>
        /// Applies formatting to a custom error message, based on the data field
        /// where the error occurred.
        /// </summary>
        /// <param name="name">
        /// The name to include in the formatted message.
        /// </param>
        /// <param name="customError">
        /// The value of custom error message.
        /// </param>
        /// <returns>
        /// A <see cref="string "/> instance of the formatted error message.
        /// </returns>
        public string FormatCustomErrorMessgae(string name, string customError)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return customError != null
                ? string.Format(CultureInfo.CurrentCulture, customError, name)
                : FormatErrorMessage(name);
        }

        /// <summary>
        /// Checks if the value is a valid business activity identifier
        /// in one of various forms: KRS, NIP or REGON.
        /// </summary>
        /// <param name="value">
        /// The data field value to validate.
        /// </param>
        /// <param name="validationContext">
        /// The <see cref="ValidationContext"/>.
        /// </param>
        /// <returns>
        ///  An instance of the <see cref="ValidationContext"/> class.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="validationContext"/> is <see langword="null"/>.
        /// </exception>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (validationContext == null)
            {
                throw new ArgumentNullException(nameof(validationContext));
            }

            if (!(value is string id))
            {
                // Not a string
                return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
            }

            if (id.Length < 9 || id.Length > 13 || id.Length == 11)
            {
                // Wrong length
                return new ValidationResult(FormatCustomErrorMessgae(validationContext.DisplayName, InvalidLengthErrorMessage));
            }

            // Check if valid REGON
            if (id.Length == 9)
            {
                if (id.All(sign => char.IsDigit(sign)))
                {
                    return ValidationResult.Success;
                }
                else
                {
                    // Non-digits in REGON
                    return new ValidationResult(FormatCustomErrorMessgae(validationContext.DisplayName, NonDigitsFoundErrorMessage));
                }
            }

            // Check if valid NIP or KRS
            if (id.Length == 10)
            {
                if (id.All(sign => char.IsDigit(sign)))
                {
                    return ValidationResult.Success;
                }
                else
                {
                    // Non-digits in NIP or KRS
                    return new ValidationResult(FormatCustomErrorMessgae(validationContext.DisplayName, NonDigitsFoundErrorMessage));
                }
            }

            // Check if valid NIP with two leading country letters - european format
            if (id.Length == 12)
            {
                if (id.Substring(0, 2).Equals("PL", StringComparison.OrdinalIgnoreCase)
                    && id.Substring(2).All(sign => char.IsDigit(sign)))
                {
                    return ValidationResult.Success;
                }
                else
                {
                    // Wrong NIP format
                    return new ValidationResult(FormatCustomErrorMessgae(validationContext.DisplayName, InvalidEuropeanFormatErrorMessage));
                }
            }

            // Check if valid NIP for dashes pattern
            if (Regex.IsMatch(id, BusinessNipPatter) || Regex.IsMatch(id, IndividualNipPatter))
            {
                return ValidationResult.Success;
            }
            else
            {
                // No match for NIP pattern with dashes in between
                return new ValidationResult(FormatCustomErrorMessgae(validationContext.DisplayName, InvalidFormatErrorMessage));
            }
        }
    }
}
