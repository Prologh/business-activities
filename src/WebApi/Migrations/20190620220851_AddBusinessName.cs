﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BusinessActivities.WebApi.Migrations
{
    public partial class AddBusinessName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "BusinessActivity",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "BusinessActivity");
        }
    }
}
