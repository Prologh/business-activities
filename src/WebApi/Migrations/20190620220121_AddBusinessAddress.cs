﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BusinessActivities.WebApi.Migrations
{
    public partial class AddBusinessAddress : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "City",
                table: "BusinessActivity",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostalCode",
                table: "BusinessActivity",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Street",
                table: "BusinessActivity",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StreetNumber",
                table: "BusinessActivity",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "City",
                table: "BusinessActivity");

            migrationBuilder.DropColumn(
                name: "PostalCode",
                table: "BusinessActivity");

            migrationBuilder.DropColumn(
                name: "Street",
                table: "BusinessActivity");

            migrationBuilder.DropColumn(
                name: "StreetNumber",
                table: "BusinessActivity");
        }
    }
}
