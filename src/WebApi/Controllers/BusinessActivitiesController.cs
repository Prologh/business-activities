﻿using BusinessActivities.Models;
using BusinessActivities.WebApi.ComponentModel.DataAnnotations;
using BusinessActivities.WebApi.Filters;
using BusinessActivities.WebApi.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BusinessActivities.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BusinessActivitiesController : ControllerBase
    {
        private readonly IRepository<BusinessActivity> _activities;

        public BusinessActivitiesController(IRepository<BusinessActivity> activities)
        {
            _activities = activities;
        }

        // GET api/BusinessActivities/1234567890
        /// <summary>
        /// Gets single business activity by its KRS, NIP or REGON number.
        /// </summary>
        /// <param name="id">
        /// The KRS, NIP or REGON number.
        /// </param>
        [HttpGet("{id}")]
        [RequestLogging("id")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<BusinessActivity>> Get([FromRoute][BusinessActivityId] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var businessActivity = await _activities.FindAsync(id);
            if (businessActivity == null)
            {
                return NotFound();
            }
            else
            {
                return businessActivity;
            }
        }
    }
}
