﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace BusinessActivities.WebApp.Services
{
    public class BusinessActivitiesManager
    {
        private const string ApiPathSegment = "BusinessActivities";

        public BusinessActivitiesManager(IConfiguration configuration, IHttpClientFactory httpClientFactory)
        {
            BaseUri = new Uri(configuration.GetValue<string>("ApiConfiguration:Uri"));
            HttpClient = httpClientFactory.CreateClient();
        }

        /// <summary>
        /// Gets requests base URI.
        /// </summary>
        protected Uri BaseUri { get; }

        /// <summary>
        /// Gets the currently used instance of <see cref="System.Net.Http.HttpClient"/>.
        /// </summary>
        protected HttpClient HttpClient { get; }

        /// <summary>
        /// Asynchronously send a HTTP GET request to the request URI
        /// and deserializes to strongly type object.
        /// </summary>
        /// <param name="id">
        ///
        /// </param>
        /// <returns>
        /// The <see cref="Task{TResult}"/> object representing the
        /// asynchronous operation.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="id"/> is <see langword="null"/>.
        /// </exception>
        public async Task<(HttpResponseMessage, TEntity)> GetAsync<TEntity>(string id)
        {
            if (id == null)
            {
                throw new ArgumentNullException(nameof(id));
            }

            var uri = $"{BaseUri.AbsoluteUri}{ApiPathSegment}/{id}";
            var resposne = await HttpClient.GetAsync(uri);
            if (resposne.IsSuccessStatusCode)
            {
                var json = await resposne.Content.ReadAsStringAsync();
                var model = JsonConvert.DeserializeObject<TEntity>(json);

                return (resposne, model);
            }
            else
            {
                return (resposne, default(TEntity));
            }
        }
    }
}
