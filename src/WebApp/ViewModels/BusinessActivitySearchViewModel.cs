﻿using System.ComponentModel.DataAnnotations;

namespace BusinessActivities.WebApp.ViewModels
{
    /// <summary>
    /// Represents view model for searching for business activity.
    /// </summary>
    public class BusinessActivitySearchViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessActivitySearchViewModel"/>
        /// class.
        /// </summary>
        public BusinessActivitySearchViewModel()
        {

        }

        /// <summary>
        /// Gets or sets the city name.
        /// </summary>
        [Display(Name = "Miasto")]
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the id used for searching.
        /// </summary>
        [Display(Name = "KRS, NIP lub REGON")]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the National Court Register (KRS) number.
        /// <para/>
        /// KRS is a ten-digit number from public register, maintained
        /// by selected district courts and Polish Ministry of Justice.
        /// </summary>
        [Display(Name = "KRS")]
        public string Krs { get; set; }

        /// <summary>
        /// Gets or sets the business name.
        /// </summary>
        [Display(Name = "Nazwa")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Tax Identification Number (NIP).
        /// <para/>
        /// NIP is a ten-digit code used to identify taxpayers in Poland.
        /// </summary>
        [Display(Name = "NIP")]
        public string Nip { get; set; }

        /// <summary>
        /// Gets or sets the postal code.
        /// </summary>
        [Display(Name = "Kod pocztowy")]
        public string PostalCode { get; set; }

        /// <summary>
        /// Gets or sets the Register of National Economy (REGON) number.
        /// <para/>
        /// REGON is a nine-digit number of subject from Register of National Economy.
        /// </summary>
        [Display(Name = "REGON")]
        public string Regon { get; set; }

        /// <summary>
        /// Gets or sets the name of the street.
        /// </summary>
        [Display(Name = "Ulica")]
        public string Street { get; set; }

        /// <summary>
        /// Gets or sets the street number.
        /// </summary>
        [Display(Name = "Numer")]
        public string StreetNumber { get; set; }
    }
}
