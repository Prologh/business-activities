﻿namespace BusinessActivities.WebApp.ViewModels
{
    public class BusinessActivityIdViewModel
    {
        public string Id { get; set; }
    }
}
