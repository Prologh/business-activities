﻿$(document).ready(() => {

    const url = $('#searchForm').attr('action');
    const $model = $('#model');
    const $searchInput = $('#searchInput');
    const $sendButton = $('#sendButton');
    const $errorMessage = $('#errorMessage');

    var fetchInitiated = false;

    $searchInput.on('input paste cut', () => {
        fetch();
    });

    $sendButton.click(() => {
        fetch();
    });

    function fetch() {

        // Clean inputs.
        $model.find('input').val('');

        // Clean error
        $errorMessage.text('');

        // Manually force jQuery validation
        if (!$searchInput.valid()) {
            return;
        }

        let searchValue = $searchInput.val();
        if (!searchValue || fetchInitiated) {
            return;
        }

        // Disallow next requests.
        fetchInitiated = true;

        $.ajax({
            url: url + '?' + $searchInput.attr('name') + '=' + searchValue,
            type: 'GET'
        }).done((data) => {

            // Fill inputs with data.
            $model.html($(data).filter('#modelBody').html());

            // Fill error message (if exists).
            $errorMessage.html($(data).filter('#errorMessageBody').html());

        }).fail(() => {
            // Show error message.
            $errorMessage.text('Zły format.');
        }).always(() => {
            // Allow next requests.
            fetchInitiated = false;
        });
    }

});
