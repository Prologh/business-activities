﻿using BusinessActivities.WebApp.ViewModels;
using FluentValidation;

namespace BusinessActivities.WebApp.Validators
{
    public class BusinessActivitySearchViewModelValidator : AbstractValidator<BusinessActivitySearchViewModel>
    {
        public BusinessActivitySearchViewModelValidator()
        {
            RuleFor(b => b.Id)
                .NotEmpty()
                .WithMessage("To pole nie może być puste.");
            RuleFor(b => b.Id)
                .MinimumLength(9)
                .WithMessage("To pole musi zawierać przynajmniej {0} znaków.");
            RuleFor(b => b.Id)
                .MaximumLength(13)
                .WithMessage("To pole nie może zawierać więcej niż {0} znaków.");
        }
    }
}
