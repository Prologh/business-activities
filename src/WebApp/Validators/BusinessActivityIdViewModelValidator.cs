﻿using BusinessActivities.WebApp.ViewModels;
using FluentValidation;

namespace BusinessActivities.WebApp.Validators
{
    public class BusinessActivityIdViewModelValidator : AbstractValidator<BusinessActivityIdViewModel>
    {
        public BusinessActivityIdViewModelValidator()
        {
            RuleFor(b => b.Id)
                .NotEmpty();
            RuleFor(b => b.Id)
                .MinimumLength(9);
            RuleFor(b => b.Id)
                .MaximumLength(13);
        }
    }
}
