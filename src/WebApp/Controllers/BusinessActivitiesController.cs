﻿using BusinessActivities.WebApp.Services;
using BusinessActivities.WebApp.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net;
using System.Threading.Tasks;

namespace BusinessActivities.WebApp.Controllers
{
    public class BusinessActivitiesController : Controller
    {
        private readonly BusinessActivitiesManager _manager;

        public BusinessActivitiesController(BusinessActivitiesManager manager)
        {
            _manager = manager;
        }

        [HttpGet]
        public async Task<IActionResult> Index([FromQuery] BusinessActivityIdViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var (response, resultViewModel) = await _manager.GetAsync<BusinessActivitySearchViewModel>(viewModel.Id);
            if (response.StatusCode == HttpStatusCode.NotFound)
            {
                ViewBag.ErrorMessage = "Brak wyników.";
            }
            if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                ViewBag.ErrorMessage = "Zły format.";
            }

            return PartialView("_Model", resultViewModel);
        }
    }
}
